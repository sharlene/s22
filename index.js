/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

console.log('Below are the original set of registered users');
console.log(registeredUsers);

console.log('Below are the original list of friends');
console.log(friendsList);

//Register

    function registerUser() {
        const userName = prompt("To register a new user, please enter a username:");

        // Split the entered username into an array of words
        const words = userName.split(' ');

        // Capitalize the first letter of each word
        const formattedUserName = words.map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');

        if (registeredUsers.includes(formattedUserName)) {
            alert("Registration failed. Username already exists!");
        } else {
            registeredUsers.push(formattedUserName);
            alert("Thank you for registering!");
        }
    }

    // Prompt the user to register at least once
    registerUser();

    let keepRegistering = true;
    while (keepRegistering) {
        const continuePrompt = prompt("Do you want to register another user? (y/n)").toLowerCase();
        if (continuePrompt === "y") {
            registerUser();
        } else if (continuePrompt === "n") {
            keepRegistering = false;
        } else {
            alert("Invalid input. Please enter 'y' or 'n'.");
        }
    }

    // Log all the new users that were registered
    console.log('New users registered:');
    for (let i = registeredUsers.length - 1; i > 6; i--) {
      console.log(registeredUsers[i]);
    }

    // Log the current set of registered users.
    console.log('Below are the current set of registered users');
console.log(registeredUsers);


// Add friend to friendlist
    function addFriend() {
      const friendName = prompt("To add a friend, please enter their username:");
      const parts = friendName.split(' ');
      const capitalizedParts = parts.map(part => part.charAt(0).toUpperCase() + part.slice(1));
      const capitalizedFriendName = capitalizedParts.join(' ');

      if (friendsList.includes(capitalizedFriendName)) {
        alert(`You are already friends with ${capitalizedFriendName}!`);
      } else {
        if (registeredUsers.includes(capitalizedFriendName)) {
          friendsList.push(capitalizedFriendName);
          alert(`You have added ${capitalizedFriendName} as a friend!`);
        } else {
          alert("User not found.");
        }
      }
    }

    // invoke and add a friend
    addFriend();

    // loop to add more friends
    let keepAddingFriends = true;
    while (keepAddingFriends) {
      const response = prompt("Do you want to add a friend? (y/n)").toLowerCase();
      if (response === "y") {
        addFriend();
      } else if (response === "n") {
        keepAddingFriends = false;
      } else {
        alert("Invalid input. Please enter 'y' or 'n'.");
      }
    }

    // log the friendsList array in the console.
    console.log("Your friends list:");
    console.log(friendsList);


// Diplay friends - list names
    console.log('Here is a list of your added friends');
    function displayFriendsList() {
      if (friendsList.length === 0) {
        alert("You currently have 0 friends. Add one first.");
      } else {
        friendsList.forEach(function(friend, index) {
          console.log(index + 1 + ". " + friend);
        });
      }
    }

    // invoke the function
    displayFriendsList();

// Display number of friends

    function displayNumOfFriends() {
      if (friendsList.length === 0) {
        alert("You currently have 0 friends. Add one first.");
      } else {
        alert(`You currently have ${friendsList.length} friends.`);
      }
    }

    // invoke the function
    displayNumOfFriends();

// Delete last element

    function deleteUser() {
      if (friendsList.length === 0) {
        alert("You currently have 0 friends. Add one first.");
        return;
      }

      let userIndex = parseInt(prompt(`Enter the index of the user you want to delete (1-${friendsList.length}):`));

      while (isNaN(userIndex) || userIndex < 1 || userIndex > friendsList.length) {
        userIndex = parseInt(prompt(`Invalid input. Enter the index of the user you want to delete (1-${friendsList.length}):`));
      }

      const deletedUser = friendsList.splice(userIndex - 1, 1)[0];

      alert(`${deletedUser} has been deleted from your friends list.`);

      console.log(`Removed from your friends list: ${deletedUser}`);
    }

    // invoke the function
    deleteUser();

    // log the updated friendsList array
    console.log("Your current friends list:");
    console.log(friendsList);

// option to add new registered users via browser console.
console.log("To register new users again, type in the console the following: \n registerUser()");
console.log("To view all registered users again, type in the console the following: \n console.log(registeredUsers)");
// option to add new friends via browser console.
console.log("To add new friends again, type in the console the following: \n addFriend()");
console.log("To view all your friends again, type in the console the following: \n console.log(friendsList)");
// option to view friends list via browser console.
console.log("To dispaly your added friends again, type in the console the following: \n displayFriendsList()");
// option to delete friends via browser console.
console.log("To delete more friends, type in the console the following: \n deleteUser()");